//
// Created by mazin on 4/11/21.
//

#include "ModuleConfigs.h"
#include "CameraConfigs.h"
#include "Calibration3D.h"
#include <iostream>
#include <Calibration2D.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>
#include <pcl/filters/crop_box.h>


void printConfig(const CameraConfigs &configs);

void test_depth()
{
    ModuleConfigs moduleConfigs;
    moduleConfigs.loadConfigs("/home/mazin/CLionProjects/vision/ModuleConfig/Configs/module_config.ini");
    std::vector<std::string> camera_configs;
    camera_configs.push_back(moduleConfigs.depth_camera_config_file_path_);
    for (const auto &camera_config : camera_configs)
    {
        CameraConfigs configs = CameraConfigs();
        configs.loadConfigs(camera_config);
        printConfig(configs);
        Calibration3D calibration_3_d(configs.getCameraCalibratioConfigFilePath());
        calibration_3_d.calculateHomographyMatrix();
        std::cout << "Calib Mat" << std::endl;
        Eigen::MatrixXf hm = calibration_3_d.getHomographyMatrix();
        configs.setCameraHomographyMatrix(calibration_3_d.getHomographyMatrix());
        printConfig(configs);
        configs.saveConfigs(camera_config);
        std::cout << hm << std::endl;
    }
}

void printConfig(const CameraConfigs &configs)
{
    std::cout << "Serial Number " << std::endl << configs.getSerialnum() << std::endl;
    std::cout << "position " << std::endl << configs.getPosition() << std::endl;
    std::cout << "camera init config file path " << std::endl << configs.getCameraInitConfigfilepath() << std::endl;
    std::cout << "camera calib config file path " << std::endl << configs.getCameraCalibratioConfigFilePath() << std::endl;
    std::cout << "IP " << std::endl << configs.getIp() << std::endl;
    std::cout << "Homogrophy Matrix CV " << std::endl << configs.getCameraHomographyCVMat() << std::endl;
    std::cout << "Homogrophy Matrix Eigen " << std::endl << configs.getCameraHomographyEigenMat() << std::endl;
}

void test_ir()
{
    ModuleConfigs moduleConfigs;
    moduleConfigs.loadConfigs("/home/mazin/CLionProjects/vision/ModuleConfig/Configs/module_config.ini");
    std::vector<std::string> camera_configs;
    camera_configs.push_back(moduleConfigs.ir_camera_config_file_path_);

    for (const auto &camera_config : camera_configs)
    {
        CameraConfigs configs = CameraConfigs();
        configs.loadConfigs(camera_config);
        printConfig(configs);
        Calibration2D calibration_2_d(configs.getCameraCalibratioConfigFilePath());
        calibration_2_d.calculateHomographyMatrix();
        configs.setCameraHomographyMatrix(calibration_2_d.getHomographyMatrix());
        printConfig(configs);
        configs.saveConfigs(camera_config);

        cv::Mat hm = calibration_2_d.getHomographyMatrix();
        std::vector<cv::Point2f> img_coor;
        img_coor.push_back(cv::Point2f(412, 281));//A3
        img_coor.push_back(cv::Point2f(416, 613));//B3
        img_coor.push_back(cv::Point2f(732, 614));//D3
        img_coor.push_back(cv::Point2f(748, 287));//C3
        std::vector<cv::Point2f> world_coor(img_coor.size());
        cv::perspectiveTransform(img_coor, world_coor, hm);
        for (const auto &coor:world_coor)
        {
            std::cout << "ir world _ point " << coor << std::endl;
        }
    }
}

void test_rgb()
{
    ModuleConfigs moduleConfigs;
    moduleConfigs.loadConfigs("/home/mazin/CLionProjects/vision/ModuleConfig/Configs/module_config.ini");
    std::vector<std::string> camera_configs;
    camera_configs.push_back(moduleConfigs.rgb_camera_config_file_path_);

    for (const auto &camera_config : camera_configs)
    {
        CameraConfigs configs = CameraConfigs();
        configs.loadConfigs(camera_config);
        printConfig(configs);
        Calibration2D calibration_2_d(configs.getCameraCalibratioConfigFilePath());
        calibration_2_d.calculateHomographyMatrix();
        configs.setCameraHomographyMatrix(calibration_2_d.getHomographyMatrix());
        printConfig(configs);
        configs.saveConfigs(camera_config);


        cv::Mat hm = calibration_2_d.getHomographyMatrix();
        std::vector<cv::Point2f> img_coor;

        img_coor.push_back(cv::Point2f(599, 493));//A3
        img_coor.push_back(cv::Point2f(584, 994));//B3
        img_coor.push_back(cv::Point2f(1085, 995));//D3
        img_coor.push_back(cv::Point2f(1110, 497));//C3

        std::vector<cv::Point2f> world_coor(img_coor.size());
        cv::perspectiveTransform(img_coor, world_coor, hm);

        for (const auto &coor:world_coor)
        {
            std::cout << "rgb world _ point " << coor << std::endl;
        }
    }
}
int main(int argc, char *argv[])
{
    test_rgb();
//    test_ir();
//    test_depth();
    return 0;
}
