//
// Created by mazin on 3/28/21.
//

#ifndef MODULECONFIG_MODULECONFIGS_H
#define MODULECONFIG_MODULECONFIGS_H

#include <string>
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

class ModuleConfigs
{

public:
    std::string ir_camera_config_file_path_, depth_camera_config_file_path_, rgb_camera_config_file_path_, calibration_config_file_path_;

    void loadConfigs(const std::string &);
    void saveConfigs(const std::string &);
};


#endif //MODULECONFIG_MODULECONFIGS_H
