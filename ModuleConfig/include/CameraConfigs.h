//
// Created by mazin on 3/28/21.
//

#ifndef MODULECONFIG_CAMERACONFIGS_H
#define MODULECONFIG_CAMERACONFIGS_H

#include <string>
#include <opencv2/core.hpp>
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <Eigen/Core>

class CameraConfigs {
    std::string serial_number_ = "123456";
    std::string camaera_init_config_file_path_ = "path/to/file/configs";
    std::string camera_calibration_config_file_path_ = "path/to/file/configs";
    int orientation_ = -1;
    cv::Mat camera_homography_matrix_;
    int position_ = -1;
    std::string ip_ = "dummy ip address";

public:
    CameraConfigs() {}

    ~CameraConfigs() {}

    void loadConfigs(const std::string &);

    void saveConfigs(const std::string &);

    int getPosition() const;

    const std::string &getSerialnum() const;

    const std::string &getCameraInitConfigfilepath() const;

    int getOrientation() const;

    cv::Mat getCameraHomographyCVMat() const;

    Eigen::MatrixXf getCameraHomographyEigenMat() const;

    void setCameraHomographyMatrix(const cv::Mat &camera_homography_matrix);

    void setCameraHomographyMatrix(const Eigen::MatrixXf &camera_homography_matrix);

    void setPosition(int position);

    void setIp(const std::string &ip);

    void setSerialNumber(const std::string &serial_number);

    void setConfigFilePath(const std::string &config_file_path);

    void setOrientation(int orientation);

    const std::string &getIp() const;

    const std::string &getCameraCalibratioConfigFilePath() const;

    void setCameraCalibratioConfigFilePath(const std::string &cameraCalibratioConfigFilePath);
};


#endif //MODULECONFIG_CAMERACONFIGS_H
