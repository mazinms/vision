//
// Created by mazin on 3/28/21.
//

#include "ModuleConfigs.h"

void ModuleConfigs::loadConfigs(const std::string &config_file_path)
{
    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::read_ini(config_file_path, pt);
    this->ir_camera_config_file_path_ = pt.get<std::string>("config_file_path.ir_camera");
    this->rgb_camera_config_file_path_ = pt.get<std::string>("config_file_path.rgb_camera");
    this->depth_camera_config_file_path_ = pt.get<std::string>("config_file_path.depth_camera");
}

void ModuleConfigs::saveConfigs(const std::string &config_file_path)
{
    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::write_ini(config_file_path, pt);
    pt.put("config_file_path.ir_camera", this->ir_camera_config_file_path_);
    pt.put("config_file_path.rgb_camera", this->rgb_camera_config_file_path_);
    pt.put("config_file_path.depth_camera", this->depth_camera_config_file_path_);
    pt.put("config_file_path.calibration", this->calibration_config_file_path_);
}
