//
// Created by mazin on 3/28/21.
//

#include "CameraConfigs.h"
#include <boost/algorithm/string.hpp>
#include "opencv2/core/eigen.hpp"

void CameraConfigs::saveConfigs(const std::string & config_file_path)
{
    boost::property_tree::ptree pt;
    pt.put("ID.serial_number",this->serial_number_);
    pt.put("ID.position",this->position_);
    pt.put("COMS.IP",this->ip_);
    pt.put("INIT_CONFIGS.camera_init_config_file_path",this->camaera_init_config_file_path_);
    pt.put("INIT_CONFIGS.camera_calibration_config_file",this->camera_calibration_config_file_path_);
    cv::Mat oneRow = this->camera_homography_matrix_.reshape(0,1);    // Treat as vector
    std::ostringstream os;
    os << oneRow;                             // Put to the stream
    std::string asStr = os.str();
    pt.put("MAPPING.homography_matrix_rows",this->camera_homography_matrix_.rows);
    pt.put("MAPPING.homography_matrix_cols",this->camera_homography_matrix_.cols);
    pt.put("MAPPING.homography_matrix",asStr);
    boost::property_tree::ini_parser::write_ini(config_file_path, pt);
}



void CameraConfigs::loadConfigs(const std::string & config_file_path)
{
    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::read_ini(config_file_path, pt);
    this->serial_number_ = pt.get<std::string>("ID.serial_number");
    this->position_ = pt.get<int>("ID.position");
    this->ip_ = pt.get<std::string>("COMS.IP");
    this->camaera_init_config_file_path_ = pt.get<std::string>("INIT_CONFIGS.camera_init_config_file_path");
    this->camera_calibration_config_file_path_ = pt.get<std::string>("INIT_CONFIGS.camera_calibration_config_file");

    std::string asStr = pt.get<std::string>("MAPPING.homography_matrix");
    int rows = pt.get<int>("MAPPING.homography_matrix_rows");
    int cols = pt.get<int>("MAPPING.homography_matrix_cols");
    asStr.erase(std::remove_if(asStr.begin(),asStr.end(),[](const unsigned char element){
        return element==' '||element==']'||element=='['||element==':';
    }),asStr.end());

    std::vector<std::string> v_strings;
    boost::split(v_strings,asStr,boost::is_any_of(","));
    std::vector<double> v_doubles;
    for(const std::string &element: v_strings){
        v_doubles.push_back(std::stod(element));
    }
    this->camera_homography_matrix_ = cv::Mat(v_doubles, true);
    this->camera_homography_matrix_ = this->camera_homography_matrix_.reshape(0,rows);
}

int CameraConfigs::getPosition() const
{
    return position_;
}

const std::string &CameraConfigs::getSerialnum() const
{
    return serial_number_;
}

const std::string &CameraConfigs::getCameraInitConfigfilepath() const
{
    return camaera_init_config_file_path_;
}

int CameraConfigs::getOrientation() const
{
    return orientation_;
}

cv::Mat CameraConfigs::getCameraHomographyCVMat() const
{
    return camera_homography_matrix_.clone();
}

Eigen::MatrixXf CameraConfigs::getCameraHomographyEigenMat() const
{
    Eigen::MatrixXf eigen_mat = Eigen::MatrixXf(this->camera_homography_matrix_.rows, this->camera_homography_matrix_.cols);
    cv::cv2eigen(this->camera_homography_matrix_, eigen_mat);
    return eigen_mat;
}

void CameraConfigs::setCameraHomographyMatrix(const cv::Mat &camera_homography_matrix)
{
    camera_homography_matrix_ = camera_homography_matrix;
}

void CameraConfigs::setCameraHomographyMatrix(const Eigen::MatrixXf &camera_homography_matrix)
{
    cv::eigen2cv(camera_homography_matrix,this->camera_homography_matrix_);
}
void CameraConfigs::setPosition(int position)
{
    position_ = position;
}

void CameraConfigs::setIp(const std::string &ip)
{
    ip_ = ip;
}

void CameraConfigs::setSerialNumber(const std::string &serial_number)
{
    serial_number_ = serial_number;
}

void CameraConfigs::setConfigFilePath(const std::string &config_file_path)
{
    camaera_init_config_file_path_ = config_file_path;
}

void CameraConfigs::setOrientation(int orientation)
{
    orientation_ = orientation;
}

const std::string &CameraConfigs::getIp() const {
    return ip_;
}

const std::string &CameraConfigs::getCameraCalibratioConfigFilePath() const {
    return camera_calibration_config_file_path_;
}

void CameraConfigs::setCameraCalibratioConfigFilePath(const std::string &cameraCalibratioConfigFilePath) {
    camera_calibration_config_file_path_ = cameraCalibratioConfigFilePath;
}
