//
// Created by mazin on 3/28/21.
//

#include <iostream>
#include <string>
#include "ModuleConfigs.h"
#include "CameraConfigs.h"
#include <vector>

int main(int ac, char **av)
{
        ModuleConfigs moduleConfigs;
        moduleConfigs.loadConfigs("/home/mazin/CLionProjects/vision/ModuleConfig/Configs/module_config.ini");
        std::vector<std::string> camera_configs;
        camera_configs.push_back(moduleConfigs.ir_camera_config_file_path_);
        camera_configs.push_back(moduleConfigs.depth_camera_config_file_path_);
        camera_configs.push_back(moduleConfigs.rgb_camera_config_file_path_);

        for(const auto &camera_config : camera_configs){
            CameraConfigs configs = CameraConfigs();
            configs.loadConfigs(camera_config);
            std::cout << "Serial Number " << configs.getSerialnum() << std::endl;
            std::cout << "position " << configs.getPosition() << std::endl;
            std::cout << "camera init config file path " << configs.getCameraInitConfigfilepath() << std::endl;
            std::cout << "camera calib config file path " << configs.getCameraCalibratioConfigFilePath() << std::endl;
            std::cout << "IP " << configs.getIp() << std::endl;
            std::cout << "Homogrophy Matrix CV " << configs.getCameraHomographyCVMat() << std::endl;
            std::cout << "Homogrophy Matrix Eigen " << configs.getCameraHomographyEigenMat() << std::endl;
            std::cout << std::endl;
            std::cout << std::endl;
        }

    return 0;
}