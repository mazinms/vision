//
// Created by mazin on 4/3/21.
//

#ifndef CALIBRATION_CALIBRATION3D_H
#define CALIBRATION_CALIBRATION3D_H

#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/correspondence.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/board.h>
#include <pcl/filters/uniform_sampling.h>
#include <pcl/recognition/cg/hough_3d.h>
#include <pcl/recognition/cg/geometric_consistency.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/common/transforms.h>
#include <pcl/console/parse.h>
#include <string>
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string.hpp>


typedef pcl::PointXYZRGBA PointType;
typedef pcl::Normal NormalType;
typedef pcl::ReferenceFrame RFType;
typedef pcl::SHOT352 DescriptorType;


class Calibration3D
{
    struct Marker
    {
        Marker(pcl::PointCloud<pcl::PointXYZ>::Ptr marker_cloud, pcl::PointXYZ marker_center_xyz, bool x_axis_reflection, bool y_axis_reflection, bool z_axis_reflection) : marker_cloud(marker_cloud), marker_center_xyz(marker_center_xyz), x_axis_reflection(x_axis_reflection), y_axis_reflection(y_axis_reflection), z_axis_reflection(z_axis_reflection)
        {}

        Marker()
        {}

        pcl::PointCloud<pcl::PointXYZ>::Ptr marker_cloud;
        pcl::PointXYZ marker_center_xyz;
        bool x_axis_reflection = false;
        bool y_axis_reflection = false;
        bool z_axis_reflection = false;
    };

    Marker marker_;
    pcl::PointCloud<pcl::PointXYZ>::Ptr source_cloud_;
    Eigen::Matrix4f homography_matrix_;

    void loadMarker(const boost::property_tree::ptree &pt);

    void loadSourceCloud(const boost::property_tree::ptree &);

public:
    Calibration3D(std::string config_file_path);

    const Eigen::Matrix4f &getHomographyMatrix() const;

    void calculateHomographyMatrix();
};


#endif //CALIBRATION_CALIBRATION3D_H
