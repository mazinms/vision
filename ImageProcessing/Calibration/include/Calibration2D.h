//
// Created by mazin on 3/30/21.
//

#ifndef CALIBRATION_CALIBRATION2D_H
#define CALIBRATION_CALIBRATION2D_H

#include <string>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>

class Calibration2D
{
    struct Marker
    {
        Marker(cv::Mat image, cv::Point world_xy) : image(image), world_xy(world_xy)
        {}

        cv::Mat image;
        cv::Point world_xy, image_xy;
    };

    std::vector<Marker> markers_;
    cv::Mat source_image_, homography_matrix_;

    void loadMarkers(const boost::property_tree::ptree &);

    void loadSourceImage(const boost::property_tree::ptree &);

    cv::Point findMarker(const Marker &);

public:
    Calibration2D(std::string config_file_path);

    void findCalibrationMarkers();

    void calculateHomographyMatrix();

    const cv::Mat &getHomographyMatrix() const;
};


#endif //CALIBRATION_CALIBRATION2D_H
