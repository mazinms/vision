//
// Created by mazin on 4/3/21.
//

#include "Calibration3D.h"

void Calibration3D::loadMarker(const boost::property_tree::ptree &pt)
{
    std::string cloud_path = pt.get<std::string>("Marker.cloud_path");
    pcl::PointCloud<pcl::PointXYZ>::Ptr marker_cloud(new pcl::PointCloud<pcl::PointXYZ>());
    pcl::io::loadPCDFile(cloud_path, *marker_cloud);
    std::string world_xyz_str = pt.get<std::string>("Marker.world_coord");
    world_xyz_str.erase(std::remove_if(world_xyz_str.begin(), world_xyz_str.end(), [](const unsigned char element)
    {
        return element == ' ' || element == ']' || element == '[' || element == ':';
    }), world_xyz_str.end());
    std::vector<std::string> v_strings;
    boost::split(v_strings, world_xyz_str, boost::is_any_of(","));
    std::vector<int> v_int;
    for (const std::string &element: v_strings)
    {
        v_int.push_back(std::stoi(element));
    }
    pcl::PointXYZ marker_world_xyz = pcl::PointXYZ(v_int.at(0), v_int.at(1), v_int.at(2));
    bool x_axis_reflection = pt.get<bool>("Marker.X_axis_reflection");
    bool y_axis_reflection = pt.get<bool>("Marker.Y_axis_reflection");
    bool z_axis_reflection = pt.get<bool>("Marker.Z_axis_reflection");

    this->marker_ = Marker(marker_cloud, marker_world_xyz, x_axis_reflection, y_axis_reflection, z_axis_reflection);
}

void Calibration3D::loadSourceCloud(const boost::property_tree::ptree &pt)
{

    std::string model_filename_ = pt.get<std::string>("Source_Cloud.cloud_path");
    //
    //  Load clouds
    //
    pcl::PointCloud<pcl::PointXYZ>::Ptr placeholder_cloud(new pcl::PointCloud<pcl::PointXYZ>());

    if (pcl::io::loadPCDFile(model_filename_, *source_cloud_) < 0)
    {
        std::cout << "Error loading model cloud." << model_filename_ << std::endl;
        return;
    }
}

Calibration3D::Calibration3D(std::string config_file_path) : source_cloud_(new pcl::PointCloud<pcl::PointXYZ>())
{
    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::read_ini(config_file_path, pt);
    loadSourceCloud(pt);
    loadMarker(pt);
}

void computeNormals(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in, pcl::PointCloud<pcl::Normal>::Ptr &cloud_out)
{
    pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> norm_est;
    norm_est.setKSearch(10);
    norm_est.setInputCloud(cloud_in);
    norm_est.compute(*cloud_out);
}

void downSampleCloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out, const float cloud_ss)
{
    pcl::UniformSampling<pcl::PointXYZ> uniform_sampling;
    uniform_sampling.setInputCloud(cloud_in);
    uniform_sampling.setRadiusSearch(cloud_ss);
    uniform_sampling.filter(*cloud_out);
}

void computeDescriptor(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in, const pcl::PointCloud<pcl::Normal>::Ptr &cloud_normals, const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_keypoints, pcl::PointCloud<pcl::SHOT352>::Ptr &cloud_out)
{
    float descr_rad_(20.0f);
    pcl::SHOTEstimationOMP<pcl::PointXYZ, pcl::Normal, pcl::SHOT352> descr_est;
    pcl::PointCloud<pcl::SHOT352>::Ptr cloud_descriptors(new pcl::PointCloud<pcl::SHOT352>());
    descr_est.setRadiusSearch(descr_rad_);
    descr_est.setInputCloud(cloud_keypoints);
    descr_est.setInputNormals(cloud_normals);
    descr_est.setSearchSurface(cloud_in);
    descr_est.compute(*cloud_out);
}

void findCloudsCorrespondences(const pcl::PointCloud<pcl::SHOT352>::Ptr &cloud_1_descriptors_in, const pcl::PointCloud<pcl::SHOT352>::Ptr &cloud_2_descriptors_in, pcl::CorrespondencesPtr &cloud_corrs_out)
{
    pcl::KdTreeFLANN<pcl::SHOT352> match_search;
    match_search.setInputCloud(cloud_1_descriptors_in);

    //  For each scene keypoint descriptor, find nearest neighbor into the model keypoints descriptor cloud and add it to the correspondences vector.
    for (std::size_t i = 0; i < cloud_2_descriptors_in->size(); ++i)
    {
        std::vector<int> neigh_indices(1);
        std::vector<float> neigh_sqr_dists(1);
        if (!std::isfinite(cloud_2_descriptors_in->at(i).descriptor[0])) //skipping NaNs
        {
            continue;
        }
        int found_neighs = match_search.nearestKSearch(cloud_2_descriptors_in->at(i), 1, neigh_indices, neigh_sqr_dists);
        if (found_neighs == 1 && neigh_sqr_dists[0] < 0.30f) //  add match only if the squared descriptor distance is less than 0.25 (SHOT descriptor distances are between 0 and 1 by design)
        {
            pcl::Correspondence corr(neigh_indices[0], static_cast<int> (i), neigh_sqr_dists[0]);
            cloud_corrs_out->push_back(corr);
        }
    }
}

void Calibration3D::calculateHomographyMatrix()
{
    //  Compute Normals
    pcl::PointCloud<pcl::Normal>::Ptr source_cloud_normals(new pcl::PointCloud<pcl::Normal>());
    pcl::PointCloud<pcl::Normal>::Ptr marker_cloud_normals(new pcl::PointCloud<pcl::Normal>());
    std::cout << "model normals" << std::endl;

    Eigen::Matrix4f reflection_transformation = Eigen::Matrix4f::Identity();
    if (this->marker_.x_axis_reflection)
        reflection_transformation(0, 0) = -reflection_transformation(0, 0);
    if (this->marker_.y_axis_reflection)
        reflection_transformation(1, 1) = -reflection_transformation(1, 1);
    if (this->marker_.z_axis_reflection)
        reflection_transformation(2, 2) = -reflection_transformation(2, 2);

    std::cout << reflection_transformation << std::endl;
    pcl::transformPointCloud(*this->marker_.marker_cloud, *this->marker_.marker_cloud, reflection_transformation);

    computeNormals(this->marker_.marker_cloud, marker_cloud_normals);
    std::cout << "scene normals" << std::endl;
    computeNormals(this->source_cloud_, source_cloud_normals);

    //  Downsample Clouds to Extract keypoints
    pcl::PointCloud<pcl::PointXYZ>::Ptr marker_cloud_keypoints(new pcl::PointCloud<pcl::PointXYZ>());
    pcl::PointCloud<pcl::PointXYZ>::Ptr source_cloud_keypoints(new pcl::PointCloud<pcl::PointXYZ>());
    float model_ss_(10.0f);
    downSampleCloud(this->marker_.marker_cloud, marker_cloud_keypoints, model_ss_);
    std::cout << "Model total points: " << this->marker_.marker_cloud->size() << "; Selected Keypoints: " << marker_cloud_keypoints->size() << std::endl;
    float source_ss_(17.50f);
    downSampleCloud(this->source_cloud_, source_cloud_keypoints, source_ss_);
    std::cout << "Scene total points: " << this->source_cloud_->size() << "; Selected Keypoints: " << source_cloud_keypoints->size() << std::endl;

    //  Compute Descriptor for keypoints
    pcl::SHOTEstimationOMP<pcl::PointXYZ, pcl::Normal, pcl::SHOT352> descr_est;
    pcl::PointCloud<pcl::SHOT352>::Ptr marker_cloud_descriptors(new pcl::PointCloud<pcl::SHOT352>());
    pcl::PointCloud<pcl::SHOT352>::Ptr source_cloud_descriptors(new pcl::PointCloud<pcl::SHOT352>());

    computeDescriptor(this->marker_.marker_cloud, marker_cloud_normals, marker_cloud_keypoints, marker_cloud_descriptors);
    computeDescriptor(this->source_cloud_, source_cloud_normals, source_cloud_keypoints, source_cloud_descriptors);

    //  Find Marker-Source cloud Correspondences with KdTree
    pcl::CorrespondencesPtr marker_source_cloud_corrs(new pcl::Correspondences());

    findCloudsCorrespondences(marker_cloud_descriptors, source_cloud_descriptors, marker_source_cloud_corrs);
    std::cout << "Correspondences found: " << marker_source_cloud_corrs->size() << std::endl;

    //
    //  Compute (Keypoints) Reference Frames only for Hough
    //
    pcl::PointCloud<pcl::ReferenceFrame>::Ptr marker_rf(new pcl::PointCloud<pcl::ReferenceFrame>());
    pcl::PointCloud<pcl::ReferenceFrame>::Ptr source_rf(new pcl::PointCloud<pcl::ReferenceFrame>());
    std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > marker_to_source_transformation;
    std::vector<pcl::Correspondences> clustered_corrs;
    float cg_size_(10.0f);
    float cg_thresh_(5.0f);
    pcl::GeometricConsistencyGrouping<pcl::PointXYZ, pcl::PointXYZ> gc_clusterer;
    gc_clusterer.setGCSize(cg_size_);
    gc_clusterer.setGCThreshold(cg_thresh_);
    gc_clusterer.setInputCloud(marker_cloud_keypoints);
    gc_clusterer.setSceneCloud(source_cloud_keypoints);
    gc_clusterer.setModelSceneCorrespondences(marker_source_cloud_corrs);
    gc_clusterer.recognize(marker_to_source_transformation, clustered_corrs);

    //
    //  Output results
    //
    std::cout << "Model instances found: " << marker_to_source_transformation.size() << std::endl;
    std::vector<pcl::Correspondences>::iterator clustered_corrs_max = std::max_element(clustered_corrs.begin(), clustered_corrs.end(), [](const pcl::Correspondences &a, const pcl::Correspondences &b)
    {
        return a.size() < a.size();
    });
    int max_corrs_points_idx = std::distance(clustered_corrs.begin(), clustered_corrs_max);
    this->homography_matrix_ = marker_to_source_transformation[max_corrs_points_idx].inverse();
    this->homography_matrix_ = reflection_transformation * this->homography_matrix_;
}

const Eigen::Matrix4f &Calibration3D::getHomographyMatrix() const
{
    return homography_matrix_;
}
