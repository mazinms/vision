//
// Created by mazin on 3/30/21.
//

#include "Calibration2D.h"

void Calibration2D::calculateHomographyMatrix()
{
    std::vector<cv::Point2f> image_pts, world_pts;
    for (auto &marker: this->markers_)
    {
        marker.image_xy = findMarker(marker);
        image_pts.push_back(cv::Point2f(marker.image_xy.x, marker.image_xy.y));
        world_pts.push_back(cv::Point2f(marker.world_xy.x, marker.world_xy.y));
    }
    homography_matrix_ = cv::findHomography(image_pts, world_pts);
}

void Calibration2D::findCalibrationMarkers()
{
    cv::namedWindow("window");
    for (auto &marker: this->markers_)
    {
        marker.image_xy = findMarker(marker);
        cv::Rect rect(marker.image_xy.x, marker.image_xy.y, marker.image.cols, marker.image.rows);
        cv::rectangle(source_image_, rect, cv::Scalar(0), 2);
        cv::imshow("window", source_image_);
        cv::waitKey(0);
    }
}

cv::Point Calibration2D::findMarker(const Calibration2D::Marker &marker)
{
    int result_cols = source_image_.cols - marker.image.cols + 1;
    int result_rows = source_image_.rows - marker.image.rows + 1;
    cv::Mat result;
    result.create(result_rows, result_cols, CV_32FC1);
    cv::TemplateMatchModes match_method = cv::TM_SQDIFF;
    cv::matchTemplate(source_image_, marker.image, result, match_method);
    cv::normalize(result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());
    double minVal;
    double maxVal;
    cv::Point minLoc;
    cv::Point maxLoc;
    cv::Point matchLoc;
    cv::minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat());
    /// For SQDIFF and SQDIFF_NORMED, the best matches are lower values. For all the other methods, the higher the better
    if (match_method == cv::TM_SQDIFF || match_method == cv::TM_SQDIFF_NORMED)
    {
        matchLoc = minLoc;
    }
    else
    {
        matchLoc = maxLoc;
    }
    return matchLoc;
}

void Calibration2D::loadSourceImage(const boost::property_tree::ptree &pt)
{
    std::string source_image_path = pt.get<std::string>("Source_Image.image_path");
    source_image_ = cv::imread(source_image_path, cv::IMREAD_ANYCOLOR);
}

void Calibration2D::loadMarkers(const boost::property_tree::ptree &pt)
{
    for (auto &marker: pt)
    {
        if(marker.first == "Source_Image")
            continue;
        std::string image_path = marker.second.get<std::string>("image_path");
        cv::Mat marker_image = cv::imread(image_path, cv::IMREAD_ANYCOLOR);
        std::string world_xy_str = marker.second.get<std::string>("world_coord");
        world_xy_str.erase(std::remove_if(world_xy_str.begin(), world_xy_str.end(), [](const unsigned char element)
        {
            return element == ' ' || element == ']' || element == '[' || element == ':';
        }), world_xy_str.end());
        std::vector<std::string> v_strings;
        boost::split(v_strings, world_xy_str, boost::is_any_of(","));
        std::vector<int> v_int;
        for (const std::string &element: v_strings)
        {
            v_int.push_back(std::stoi(element));
        }
        cv::Point marker_world_xy = cv::Point(v_int.at(0), v_int.at(1));
        this->markers_.push_back(Marker(marker_image, marker_world_xy));
    }
}


Calibration2D::Calibration2D(std::string config_file_path)
{
    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::read_ini(config_file_path, pt);
    loadSourceImage(pt);
    loadMarkers(pt);
}

const cv::Mat &Calibration2D::getHomographyMatrix() const
{
    return homography_matrix_;
}
